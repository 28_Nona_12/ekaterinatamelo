package com.training.et.hw1;

public class Task {
    public static void main(String[] args){
        int a = Integer.parseInt(args[0]);
        int p = Integer.parseInt(args[1]);
        double m1 = Double.parseDouble(args[2]);
        double m2 = Double.parseDouble(args[3]);
        double G = 4*java.lang.Math.pow(java.lang.Math.PI, 2)*java.lang.Math.pow(a, 3)/(java.lang.Math.pow(p, 2)*(m1+m2));
        System.out.println(G);
    }
}
